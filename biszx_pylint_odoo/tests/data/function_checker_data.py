from datetime import date

from odoo import api, fields


class Test:
    _name = 'test'

    def _other_default_a(self):
        return 'a'

    def _default_a(self):
        return 'a'

    a = fields.Char(
        string='a',
        compute='_other_compute_a',
        domain=lambda self: self._other_domain_a(),
        default=lambda self: self._other_default_a(),
    )

    b = fields.Char(
        string='a',
        compute='_other_compute_a',
        default=_other_default_a,
        search='_other_search_a',
        inverse='_other_inverse_a',
    )

    c = fields.Char(
        default='c',
        compute='_compute_c',
        search='_search_a',
        inverse='_inverse_a',
    )

    d = fields.Char(
        default=_default_a,
    )

    e = fields.Char(default=lambda self: self._default_a())

    f = fields.Char(compute=False)

    g = fields.Char(default=lambda self: date.today())

    h = fields.Char(default=lambda self: str(1))

    i = fields.Char(default=lambda _: 1)

    j = fields.Char(default=lambda self: self.env.company)

    def _other_domain_a(self):
        return []

    def _domain_a(self):
        return []

    @api.depends('b')
    def _other_compute_a():
        pass

    @api.depends('b')
    def _compute_a():
        pass

    @api.onchange('b')
    def _other_onchange_a():
        pass

    @api.onchange('b')
    def _onchange_a():
        pass

    @api.constrains('b')
    def _other_check_a():
        pass

    @api.constrains('b')
    def _check_a():
        pass
