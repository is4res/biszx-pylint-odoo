from odoo import fields


class Test:
    _name = 'test'

    """
    relation2one
    """
    a = fields.Many2one()
    a_id = fields.Many2one()
    a_uid = fields.Many2one()
    a_uid = fields.Many2one(lambda: True)
    a_user_id = fields.Many2one('res.users')
    a_uid = fields.Many2one('res.users')
    a_uid = fields.Many2one(comodel_name='res.users')

    """
    relation2many
    """
    b = fields.One2many()
    b_ids = fields.One2many()
    b_uids = fields.One2many()
    b_uids = fields.One2many(lambda: True)
    b_user_ids = fields.One2many('res.users')
    b_uids = fields.One2many('res.users')
    b_uids = fields.One2many(comodel_name='res.users')
    c = fields.Many2many()
    c_ids = fields.Many2many()
    c_uids = fields.Many2many()
    c_uids = fields.Many2many(lambda: True)
    c_user_ids = fields.Many2many('res.users')
    c_uids = fields.Many2many('res.users')
    c_uids = fields.Many2many(comodel_name='res.users')

    """
    boolean
    """
    d = fields.Boolean()
    is_d = fields.Boolean()
    active = fields.Boolean()

    """
    date
    """
    e = fields.Date()
    e_date = fields.Date()
    date = fields.Date()

    """
    datetime
    """
    f = fields.Datetime()
    f_datetime = fields.Datetime()
    datetime = fields.Datetime()
