CHECKER_NAME = 'biszx-odoo'

FUNCTION_MSG_ID = 90
FIELD_MSG_ID = 91
MSG_DESCRIPTION = (
    'You can review guidelines here: '
    'https://gitlab.com/is4res/biszx-pylint-odoo/READEME.md'
)
