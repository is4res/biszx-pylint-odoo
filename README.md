# Biszx Pylint Odoo

Enable custom checks

Code | Description | short name
--- | --- | ---
C9001 | Default function name must be start with \_default\_ | biszx-default-func-name
C9002 | Search function name must be start with \_search\_ | biszx-search-func-name
C9003 | Compute function name must be start with \_compute\_ | biszx-compute-func-name
C9004 | Onchange function name must be start with \_onchange\_ | biszx-onchange-func-name
C9005 | Constrains function name must be start with \_check\_ | biszx-constrains-func-name
C9006 | Domain function name must be start with \_domain\_ | biszx-domain-func-name
C9007 | Inverse function name must be start with \_inverse\_ | biszx-inverse-func-name
C9101 | Many2one field name must be end with \_id or \_uid for res.users | biszx-relation2one-field-name
C9102 | One2many and Many2many field name must be end with \_ids or \_uids for res.users | biszx-relation2many-field-name
C9103 | Boolean field name must be start with is\_ | biszx-boolean-field-name
C9104 | Date field name must be end with \_date | biszx-date-field-name
C9105 | Datetime field name must be end with \_datetime | biszx-datetime-field-name
