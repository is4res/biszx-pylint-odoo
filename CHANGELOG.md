# Changelog


## 1.0.0 (2021-09-23)

### IMP

* [IMP]Package: update readme and version. [Isares Srirattanapaisarn]

* [IMP]FieldChecker: add condition relation for res.users. [Isares Srirattanapaisarn]

* [IMP]CHANGELOG. [Isares Srirattanapaisarn]

### Other

* Update CHANGELOG. [Biszx]


## 0.2.3 (2021-09-21)

### FIX

* [FIX]FunctionChecker: test. [Isares Srirattanapaisarn]

* [FIX]FunctionChecker: error on check api decorator. [Isares Srirattanapaisarn]


## 0.2.2 (2021-09-13)

### IMP

* [IMP]FieldChecker: datetime field; [IMP]Version. [Isares Srirattanapaisarn]


## 0.2.0 (2021-09-02)

### IMP

* [IMP]Version. [Isares Srirattanapaisarn]

* [IMP]FunctionChecker: inverse name; [FIX]FunctionChecker: error in arg.body. [Isares Srirattanapaisarn]


## 0.1.0 (2021-08-20)

### Add

* [ADD]FieldChecker. [Isares Srirattanapaisarn]

### IMP

* [IMP]Version. [Isares Srirattanapaisarn]

* [IMP]Message. [Isares Srirattanapaisarn]

* [IMP]Readme. [Isares Srirattanapaisarn]


## 0.0.7 (2021-08-20)

### IMP

* [IMP]PackageConfig. [Isares Srirattanapaisarn]


## 0.0.6 (2021-08-20)

### IMP

* [IMP]PackageConfig. [Isares Srirattanapaisarn]


## 0.0.5 (2021-08-20)

### IMP

* [IMP]PackageConfig. [Isares Srirattanapaisarn]


## 0.0.4 (2021-08-19)

### IMP

* [IMP]PackageConfig. [Isares Srirattanapaisarn]


## 0.0.3 (2021-08-19)

### IMP

* [IMP]PackageVersion. [Isares Srirattanapaisarn]


## 0.0.2 (2021-08-19)

### FIX

* [FIX]CI: add image to upload jobs. [Isares Srirattanapaisarn]


## 0.0.1 (2021-08-19)

### Other

* Add LICENSE. [Isares]

* CI change only branch test. [Isares Srirattanapaisarn]

* Update ci. [Isares Srirattanapaisarn]

* Update ci. [Isares Srirattanapaisarn]

* Update. [Isares Srirattanapaisarn]

* Init. [Isares Srirattanapaisarn]
